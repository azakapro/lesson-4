// Assignment 4
// 1st
function charReverseHandler (str) {
var UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var LOWER = 'abcdefghijklmnopqrstuvwxyz';
var result = [];
  
  for(var x=0; x<str.length; x++)
  {
    if(UPPER.indexOf(str[x]) !== -1)
    {
      result.push(str[x].toLowerCase());
    }
    else if(LOWER.indexOf(str[x]) !== -1)
    {
      result.push(str[x].toUpperCase());
    }
    else 
    {
      result.push(str[x]);
    }
  }
return result.join();
}

// 2nd



    //3rd
function findDublicatedHandler (arr) {
    console.log('Initial state: ' + arr)
    const arr3 = new Set(arr);

    const final1 = arr.filter(item => {
        if (arr3.has(item)) {
            arr3.delete(item);
        } else {
            return item;
        }
    });
    
    return [...new Set(final1)]
    
}

     
 
// 4th
function unionHandler (arr1, arr2) {
    return [...new Set([...arr1, ...arr2])];
}

//5th
function removeFunc (array) {
    let arr1 = [];
    for (let i of array) {
        if (i!='null' && i!='0' && i!='""' && i!='false'&& i!='undefined' && i!='NaN') {
            arr1.push(i);
        }
    }
    return arr1;
}


//6th
function reverseStringHandler (str) {
  return str.split("").sort().join("")
  
}
    




///////////////////////////////////////////////////////////////////////////////////////////////

// 1st
// console.log(charReverseHandler('The Quick Brown Fox'))
// 2nd

// 3rd
// console.log('Duplicates: '+findDublicatedHandler([1,2,3,4,5,5,6,7,7,8,9,77,8,55,22,77]))
// 4th
// console.log(unionHandler([1,2,7,3],[4,7,5,6]));
// 5th
// console.log(removeFunc(['null',0,2,5,false]))
// 6th
// console.log(reverseStringHandler('webmaster'))

